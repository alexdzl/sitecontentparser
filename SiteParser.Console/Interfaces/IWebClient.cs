namespace SiteParser.Console.Interfaces
{
    public interface IWebClient
    {
        string GetContent(string url);
        string GetDomain(string url);
        string GetPageAbsoluteUriWithOutName(string url);
    }
}