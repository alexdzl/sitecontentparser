namespace SiteParser.Console.Interfaces
{
    public interface IParsingLogger
    {
        void Log(string message);
    }
}