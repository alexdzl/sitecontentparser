using System.Collections.Generic;

namespace SiteParser.Console.Interfaces
{
    public interface ISiteParsingStrategy
    {
        IEnumerable<string> ParseContent(string content, string pageAbsolutePathWithoutPage);
    }

    public interface ISiteParsingStrategyDecorator: ISiteParsingStrategy
    {

    }
}