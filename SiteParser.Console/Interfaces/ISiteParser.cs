using System.Collections.Generic;

namespace SiteParser.Console.Interfaces
{
    public interface ISiteParser
    {
        IEnumerable<string> Parse(string url);
    }
}