using System.Collections.Generic;
using System.Text;
using SiteParser.Console.Interfaces;

namespace SiteParser.Console.Strategies
{
    public class SiteParsingSkipSmallPagesStrategy : ISiteParsingStrategyDecorator
    {
        private readonly ISiteParsingStrategy _strategy;
        private readonly int _pageSizeLimitKBytes = 200 * 1024;
        public SiteParsingSkipSmallPagesStrategy(ISiteParsingStrategy strategy)
        {
            _strategy = strategy;
        }
        public SiteParsingSkipSmallPagesStrategy(ISiteParsingStrategy strategy, int pageSizeLimitKBytes): this(strategy)
        {
            _pageSizeLimitKBytes = pageSizeLimitKBytes * 1024;
        }
        
        public IEnumerable<string> ParseContent(string content, string pageAbsolutePathWithoutPage)
        {
            if (Check�ondition(content))
            {
                return _strategy.ParseContent(content, pageAbsolutePathWithoutPage);
            }

            return new string[0] { };
        }

        private bool Check�ondition(string content)
        {
            return Encoding.ASCII.GetByteCount(content) > _pageSizeLimitKBytes;
        }
    }
}