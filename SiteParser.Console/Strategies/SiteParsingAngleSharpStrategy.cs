using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AngleSharp.Html.Parser;
using SiteParser.Console.Extensions;
using SiteParser.Console.Interfaces;

namespace SiteParser.Console.Strategies
{
    public class SiteParsingAngleSharpStrategy : ISiteParsingStrategy
    {
        private IParsingLogger _logger;

        public SiteParsingAngleSharpStrategy(IParsingLogger logger)
        {
            _logger = logger;
        }

        private static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public IEnumerable<string> ParseContent(string content, string pageAbsolutePathWithoutPage)// TODO get rid of pageAbsolutePath
        {
            var parser = new HtmlParser();
            var links = new HashSet<Uri>();

            using (var stream = content.AsStream())
            {
                var document = parser.ParseDocument(stream);

                foreach (var link in document.Links)
                {
                    var href = link.GetAttribute("href");
                    if (string.IsNullOrWhiteSpace(href))
                    {
                        _logger.Log("A-tag has missing or empty href");
                        continue;
                    }

                    href = href.Trim();

                    if (href[0] == '#') continue;

                    if (!href.StartsWith("http"))
                    {
                        href = pageAbsolutePathWithoutPage + href;
                    }

                    if (!Uri.IsWellFormedUriString(href, UriKind.RelativeOrAbsolute))
                    {
                        _logger.Log($"A-tag has invalid href ({href})");
                        continue;
                    }

                    try
                    {
                        links.Add(new Uri(href));
                    }
                    catch (UriFormatException exception)
                    {
                        _logger.Log($"A-tag has invalid href ({href}) error message {exception.Message}");
                    }
                }
            }



            return links.Select(uri => uri.AbsoluteUri).Distinct();
        }
    }
}