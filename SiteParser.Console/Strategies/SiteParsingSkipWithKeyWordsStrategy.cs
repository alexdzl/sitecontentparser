using System.Collections.Generic;
using System.Linq;
using SiteParser.Console.Interfaces;

namespace SiteParser.Console.Strategies
{
    public class SiteParsingSkipWithKeyWordsStrategy : ISiteParsingStrategyDecorator
    {
        private readonly string _keyWord = "auth"; //provided from outer source
        private readonly ISiteParsingStrategy _strategy;

        public SiteParsingSkipWithKeyWordsStrategy(ISiteParsingStrategy strategy)
        {
            _strategy = strategy;
        }

        public SiteParsingSkipWithKeyWordsStrategy(ISiteParsingStrategy strategy, string keyWord): this(strategy)
        {
            _keyWord = keyWord;
        }

        public IEnumerable<string> ParseContent(string content, string pageAbsolutePathWithoutPage)
        {
            return _strategy.ParseContent(content, pageAbsolutePathWithoutPage)
                .Where(link => !link.Contains(_keyWord));
        }
    }
}