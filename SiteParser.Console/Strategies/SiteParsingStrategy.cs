using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SiteParser.Console.Interfaces;

namespace SiteParser.Console.Strategies
{
    public class SiteParsingStrategy : ISiteParsingStrategy
    {
        private const string UrlPattern = "<a\\s+(?:[^>]*?\\s+)?href=([\"'])(?<url>.*?)\\1";
        private const string MatchGroupName = "url";

        public IEnumerable<string> ParseContent(string content, string pageAbsolutePathWithoutPage)
        {
            Regex htmlLinkPattern = new Regex(UrlPattern);

            var linksMatches = htmlLinkPattern.Matches(content);

            var links = linksMatches.Select(match =>
            {
                string url = match.Groups[MatchGroupName]?.Value;
                if (!url.StartsWith("http"))
                {
                    return pageAbsolutePathWithoutPage + url;
                }
                return url;
            });

            return links.Distinct();
        }
    }
}