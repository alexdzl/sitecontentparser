using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SiteParser.Console.Interfaces;

namespace SiteParser.Console.Strategies
{
    public class SiteParsingWithImagesStrategy : ISiteParsingStrategy
    {
        private const string UrlPattern = "<a\\s+(?:[^>]*?\\s+)?href=([\"'])(?<url>.*?)\\1|<img[^>]+src=\"(?<src>[^\">]+)\"";
        private const string UrlMatchGroupName = "url";
        private const string SrcMatchGroupName = "src";

        public IEnumerable<string> ParseContent(string content, string pageAbsolutePathWithoutPage)// TODO get rid of pageAbsolutePath
        {
            Regex htmlLinkPattern = new Regex(UrlPattern);
            var linksMatches = htmlLinkPattern.Matches(content);

            var links = linksMatches.Select(match =>
            {
                string url = match.Groups[UrlMatchGroupName]?.Value;
                if (string.IsNullOrEmpty(url))
                {
                    url = match.Groups[SrcMatchGroupName]?.Value;
                }

                if (!url.StartsWith("http"))
                {
                    return pageAbsolutePathWithoutPage + url;
                }
                return url;
            });

            return links.Distinct();
        }
    }
}