﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using SiteParser.Console.Interfaces;

namespace SiteParser.Console.Infrastructure
{
    public class WebClient: IWebClient
    {
        private WebClientWithTimeout client;
        public WebClient()
        {
            client = new WebClientWithTimeout();
        }
        public string GetContent(string url)
        {
            string content;
            try
            {
                content = client.DownloadStringTaskAsync(new Uri(url)).Result;
            }
            catch (Exception e)
            {
                System.Console.WriteLine($"url = {url}");
                System.Console.WriteLine($"Message = {e.Message}");
                content = string.Empty;
            }

            return content;
        }

        public string GetDomain(string url)
        {
            Uri uri = null;
            url = Regex.Replace(url, @"(https?:)\/\w", "$1/$2");
            try
            {
                uri = new Uri(url);
            }
            catch (UriFormatException exception)
            {
                System.Console.WriteLine($"url is invalid ({url}) error message {exception.Message}");
                return string.Empty;
            }
            return uri.Host;
        }

        public string GetPageAbsoluteUriWithOutName(string url)
        {
            Uri rootPageUri = new Uri(url);
            return Path.GetDirectoryName(rootPageUri.AbsoluteUri)
                .Replace("\\", "/")
                .Replace("http:/", "http://")
                ;
        }
    }
}