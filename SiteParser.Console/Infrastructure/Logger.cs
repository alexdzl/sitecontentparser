using SiteParser.Console.Interfaces;

namespace SiteParser.Console.Infrastructure
{
    public class Logger : IParsingLogger
    {
        public void Log(string message)
        {
            System.Console.WriteLine(message);
        }
    }
}