using System;
using System.Collections.Generic;
using System.Linq;
using SiteParser.Console.Interfaces;

namespace SiteParser.Console.Infrastructure
{
    public class SiteParser: ISiteParser
    {
        private readonly IWebClient _client;
        private readonly ISiteParsingStrategy _siteParsingStrategy;
        private readonly IParsingLogger _logger;

        private readonly HashSet<string> _visited;
        private readonly List<string> _aggregatedLinksList;

        private string _domain;

        public event Action<string> PageParsingStarted;

        public SiteParser(IWebClient client, ISiteParsingStrategy siteParsingStrategy, IParsingLogger logger)
        {
            _client = client;
            _siteParsingStrategy = siteParsingStrategy;
            _logger = logger;

            _visited = new HashSet<string>();
            _aggregatedLinksList = new List<string>();
        }
        public IEnumerable<string> Parse(string url)
        {
            SetSiteDomain(url);
            IEnumerable<string> links = new string[0] {};

            if (IsVisited(url))
            {
                return links;
            }

            if (_client.GetDomain(url) != _domain)
            {
                return links;
            }

            OnPageParsingStarted(url);

            links = GetLinksByUrl(url);

            _aggregatedLinksList.AddRange(links);
            SetVisited(url);

            foreach (var nestedLink in links)
            {
                Parse(nestedLink);
            }

            return _aggregatedLinksList.Distinct();
        }

        private void SetSiteDomain(string url)
        {
            if (string.IsNullOrEmpty(_domain))
            {
                _domain = _client.GetDomain(url);
            }
        }

        private bool IsVisited(string url)
        {
            url = CleanUrl(url);
            return _visited.Contains(url);
        }

        private void SetVisited(string url)
        {
            url = CleanUrl(url);
            _visited.Add(url);
        }

        private static string CleanUrl(string url)
        {
            if (url.EndsWith("/"))
            {
                url = url.Remove(url.Length - 1);
            }
            return url;
        }

        private  IEnumerable<string> GetLinksByUrl(string url)
        {
            string content = _client.GetContent(url);
            string stringPrefixForRelative = _client.GetPageAbsoluteUriWithOutName(url);
            return _siteParsingStrategy.ParseContent(content, stringPrefixForRelative);
        }

        protected virtual void OnPageParsingStarted(string url)
        {
            _logger.Log($"Parsing started for {url}");
            PageParsingStarted?.Invoke(url);
        }
    }
}