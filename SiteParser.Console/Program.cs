﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Newtonsoft.Json;
using SiteParser.Console.Infrastructure;
using SiteParser.Console.Interfaces;
using SiteParser.Console.Strategies;

namespace SiteParser.Console
{
    class Program
    {
        public static string SiteParsing = "SiteParser";

        static void Main(string[] args)
        {
            var selectedArg = "SiteParsingWithImagesStrategy";
            //var selectedArg = "SiteParsingAngleSharpStrategy";
            //var selectedArg = "SiteParsingSkipWithKeyWordsStrategy";
            var targetSite = "https://getbootstrap.com";

            IEnumerable<string> links = null;
            var container = RegisterComponents();
            try
            {
                var siteParser = container.Resolve<ISiteParser>($"{SiteParsing}{selectedArg}");
                links = siteParser.Parse(targetSite);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
                return;
            }

            var fileName = $"Links{DateTime.Now.ToFileTimeUtc().ToString()}.json";
            var serializeObjects = JsonConvert.SerializeObject(links);
            File.WriteAllText(fileName, serializeObjects);
            System.Console.WriteLine($"Parse completed check file {fileName}");
        }

        private static WindsorContainer RegisterComponents()
        {
            var strategiesTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p =>
                    typeof(ISiteParsingStrategy).IsAssignableFrom(p) 
                    && p.IsClass 
                    && !typeof(ISiteParsingStrategyDecorator).IsAssignableFrom(p)
                    ).ToList();

            var strategiesDecoratorsTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p =>
                    typeof(ISiteParsingStrategyDecorator).IsAssignableFrom(p)
                    && p.IsClass
                ).ToList();


            var container = new WindsorContainer();
            container.Register(Component
                .For<IWebClient>()
                .ImplementedBy<WebClient>().LifestyleTransient());
            container.Register(Component
                .For<IParsingLogger>()
                .ImplementedBy<Logger>().LifestyleTransient());

            foreach (var strategiesType in strategiesTypes)
            {
                container.Register(Component
                    .For<ISiteParsingStrategy>()
                    .ImplementedBy(strategiesType)
                    .LifestyleTransient().Named(strategiesType.Name));
            }

            container.Register(Component
                .For<ISiteParsingStrategy>()
                .ImplementedBy<SiteParsingStrategy>()
                .LifestyleTransient().IsDefault());

            foreach (var strategiesDecoratorsType in strategiesDecoratorsTypes)
            {

                container.Register(Component
                    .For<ISiteParsingStrategy>()
                    .ImplementedBy(strategiesDecoratorsType)
                    .LifestyleTransient().Named(strategiesDecoratorsType.Name)
                    .DependsOn(ServiceOverride.ForKey<ISiteParsingStrategy>().Eq(nameof(SiteParsingStrategy))));
            }
 
            container.Register(Component.For<ISiteParser>().ImplementedBy<Infrastructure.SiteParser>()
                .DependsOn(ServiceOverride.ForKey<ISiteParsingStrategy>().Eq(nameof(SiteParsingStrategy)))
                .Named(nameof(Infrastructure.SiteParser)+nameof(SiteParsingStrategy)));
            container.Register(Component.For<ISiteParser>().ImplementedBy<Infrastructure.SiteParser>()
                .DependsOn(ServiceOverride.ForKey<ISiteParsingStrategy>().Eq(nameof(SiteParsingWithImagesStrategy)))
                .Named(nameof(Infrastructure.SiteParser) + nameof(SiteParsingWithImagesStrategy)));
            container.Register(Component.For<ISiteParser>().ImplementedBy<Infrastructure.SiteParser>()
                .DependsOn(ServiceOverride.ForKey<ISiteParsingStrategy>().Eq(nameof(SiteParsingSkipWithKeyWordsStrategy)))
                .Named(nameof(Infrastructure.SiteParser) + nameof(SiteParsingSkipWithKeyWordsStrategy)));
            container.Register(Component.For<ISiteParser>().ImplementedBy<Infrastructure.SiteParser>()
                .DependsOn(ServiceOverride.ForKey<ISiteParsingStrategy>().Eq(nameof(SiteParsingSkipSmallPagesStrategy)))
                .Named(nameof(Infrastructure.SiteParser) + nameof(SiteParsingSkipSmallPagesStrategy)));
            container.Register(Component.For<ISiteParser>().ImplementedBy<Infrastructure.SiteParser>()
                .DependsOn(ServiceOverride.ForKey<ISiteParsingStrategy>().Eq(nameof(SiteParsingAngleSharpStrategy)))
                .Named(nameof(Infrastructure.SiteParser) + nameof(SiteParsingAngleSharpStrategy)));

            return container;
        }
    }
}
