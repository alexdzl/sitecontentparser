using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using SiteParser.Console.Interfaces;
using SiteParser.Console.Strategies;
using SiteParser.Tests.Stubs;

namespace SiteParser.Tests
{
    public class SiteParserTests
    {

        [SetUp]
        public void Setup()
        {
        }

        //relative link handling ���� ������ ���������� �� / �������� ������ �� ��������

        //add image src to urls
        //page size filter
        //page pattern exclude

        //Big O
        //AngleSharp
        //var items = document.QuerySelectorAll("a").Where(item => item.ClassName != null && item.ClassName.Contains("post__title_link"));


        [Test]
        public void SiteParser__Parsing_Target_Site__Should_Return_ListOfUniqueUrls()
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingStrategy();
            IWebClient clientStub = new WebClientStub();
            Mock<IParsingLogger> logger = new Mock<IParsingLogger>();
            ISiteParser siteParser = new Console.Infrastructure.SiteParser(clientStub, siteParsingStrategy, logger.Object);
            int expectedCount = 2;
            string expectedUrlFist = @"http://link1.html";
            string expectedUrlSecond = @"http://link2.html";

            //Arrange
            List<string> actualUrls = siteParser.Parse("http://test.com/SinglePage/pageWithTwoUniqueLinks.html").ToList();


            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount));
            
            Assert.Contains(expectedUrlFist, actualUrls);
            Assert.Contains(expectedUrlSecond, actualUrls);
        }

        [Test]
        public void SiteParser__Parsing_Target_Site__Should_Return_ListOfUniqueUrls_From_Two_Pages()
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingStrategy();
            IWebClient clientStub = new WebClientStub();
            Mock<IParsingLogger> logger = new Mock<IParsingLogger>();
            ISiteParser siteParser = new Console.Infrastructure.SiteParser(clientStub, siteParsingStrategy, logger.Object);
            int expectedCount = 6;
            string expectedUrl = @"http://test.com/RootWithTwoNestedPages/link";

            //Arrange
            List<string> actualUrls = siteParser.Parse("http://test.com/RootWithTwoNestedPages/root.html").ToList();


            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount));
            foreach (var i in Enumerable.Range(1,6))
            {
                Assert.Contains(expectedUrl + i + ".html", actualUrls);
            }
        }

        [TestCase(2, 1, 1)]
        [TestCase(10, 1, 3)]
        [TestCase(25, 17, 4)]
        [TestCase(7, 8, 2)]
        public void SiteParser__Parsing_Target_Site__Should_Return_ListOfUniqueUrls_From_ALL_Pages(int expectedCount, int linksOnPage, int externalLinksOnPage)
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingStrategy();
            IWebClient clientStub = new WebClientEmulatorStub(expectedCount, linksOnPage, externalLinksOnPage);
            Mock<IParsingLogger> logger = new Mock<IParsingLogger>();
            ISiteParser siteParser = new Console.Infrastructure.SiteParser(clientStub, siteParsingStrategy, logger.Object);
            string expectedUrl = "Link";

            //Arrange
            List<string> actualUrls = siteParser.Parse("http://test.com/Link1-1").ToList();


            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount * (linksOnPage + externalLinksOnPage)));
            foreach (var pageLevelId in Enumerable.Range(1, expectedCount))
            {
                int currentLinkId = pageLevelId + 1;
                foreach (var linkNumber in Enumerable.Range(1, linksOnPage))
                {
                    Assert.Contains($"{"http://test.com"}/{expectedUrl}{currentLinkId}-{linkNumber}", actualUrls);
                }

            }
            Assert.AreEqual(0, ((WebClientEmulatorStub)clientStub).ExternalGetContentCount);
        }


        [Test]
        public void SiteParser__Parsing_With_Include_Images__Should_Return_ListOfUniqueUrlsWithLinksFromSrcAttribute()
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingWithImagesStrategy();
            IWebClient clientStub = new WebClientStub();
            Mock<IParsingLogger> logger = new Mock<IParsingLogger>();
            ISiteParser siteParser = new Console.Infrastructure.SiteParser(clientStub, siteParsingStrategy, logger.Object);

            int expectedCount = 3;
            string expectedUrlFist = @"http://link1.html";
            string expectedUrlSecond = @"http://link2.html";
            string expectedImageSrc = @"http://test.com/SinglePagesmiley.gif";

            //Arrange
            List<string> actualUrls = siteParser.Parse("http://test.com/SinglePage/pageWithTwoUniqueLinksAndImages.html").ToList();


            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount));

            Assert.Contains(expectedUrlFist, actualUrls);
            Assert.Contains(expectedUrlSecond, actualUrls);
            Assert.Contains(expectedImageSrc, actualUrls);
        }

        [Test]
        public void SiteParser__Parsing_With_Include_Images__Should_SkipSmallSizePage()
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingSkipSmallPagesStrategy(new SiteParsingStrategy(), 200);
            IWebClient clientStub = new WebClientStub();
            Mock<IParsingLogger> logger = new Mock<IParsingLogger>();
            ISiteParser siteParser = new Console.Infrastructure.SiteParser(clientStub, siteParsingStrategy, logger.Object);
            int expectedCount = 0;


            //Arrange
            List<string> actualUrls = siteParser.Parse("http://test.com/SinglePage/pageWithTwoUniqueLinksAndImages.html").ToList();

            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount));

        }
    }
}