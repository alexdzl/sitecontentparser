using System.IO;
using System.Linq;
using NUnit.Framework;
using SiteParser.Console.Infrastructure;
using SiteParser.Console.Interfaces;
using SiteParser.Console.Strategies;

namespace SiteParser.Tests
{
    public class SiteParsingStrategyTests
    {

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void SiteParsingAngleSharpStrategy__Parsing_Content__Should_Return_ListOfUniqueUrlsWithLinks()
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingAngleSharpStrategy(new Logger());
            string expectedUrlFist = @"http://link1.html/";
            string expectedUrlSecond = @"http://link2.html/";
            string content = File.ReadAllText("HtmlSamples\\SinglePage\\pageWithTwoUniqueLinks.html");
            int expectedCount = 2;

            //Arrange
            var actualUrls = siteParsingStrategy.ParseContent(content, "/SinglePage/").ToList();


            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount));
            Assert.Contains(expectedUrlFist, actualUrls);
            Assert.Contains(expectedUrlSecond, actualUrls);
        }


        [Test]
        public void SiteParsingStrategy__Parsing_Content__Should_Return_ListOfUniqueUrlsWithLinks()
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingStrategy();
            string expectedUrlFist = @"http://link1.html";
            string expectedUrlSecond = @"http://link2.html";
            string content = File.ReadAllText("HtmlSamples\\SinglePage\\pageWithTwoUniqueLinks.html");
            int expectedCount = 2;

            //Arrange
            var actualUrls = siteParsingStrategy.ParseContent(content, "/SinglePage/").ToList();


            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount));
            Assert.Contains(expectedUrlFist, actualUrls);
            Assert.Contains(expectedUrlSecond, actualUrls);
        }

        [Test]
        public void SiteParsingWithImagesStrategy__Parsing_Content__Should_Return_ListOfUniqueUrlsWithLinksAndImages()
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingWithImagesStrategy();
            string expectedUrlFist = @"http://link1.html";
            string expectedUrlSecond = @"http://link2.html";
            string expectedImageSrc = @"/SinglePage/smiley.gif";
            string content = File.ReadAllText("HtmlSamples\\SinglePage\\pageWithTwoUniqueLinksAndImages.html");
            int expectedCount = 3;

            //Arrange
            var actualUrls = siteParsingStrategy.ParseContent(content, "/SinglePage/").ToList();


            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount));
            Assert.Contains(expectedUrlFist, actualUrls);
            Assert.Contains(expectedUrlSecond, actualUrls);
            Assert.Contains(expectedImageSrc, actualUrls);
        }

        [Test]
        public void SiteParsingSkipSmallPagesStrategy__Parsing_Content__Should_Return__ListOfUniqueUrlsWithIfPageGotCorretSize()
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingSkipSmallPagesStrategy(new SiteParsingStrategy(), 0);
            string expectedUrlFist = @"http://link1.html";
            string expectedUrlSecond = @"http://link2.html";
            string content = File.ReadAllText("HtmlSamples\\SinglePage\\pageWithTwoUniqueLinks.html");
            int expectedCount = 2;

            //Arrange
            var actualUrls = siteParsingStrategy.ParseContent(content, "/SinglePage/").ToList();

            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount));
            Assert.Contains(expectedUrlFist, actualUrls);
            Assert.Contains(expectedUrlSecond, actualUrls);
        }

        [Test]
        public void SiteParsingSkipWithKeyWordsStrategy__Parsing_Content__Should_Return__ListOfUniqueUrlsWithoutLinkWithKeyWord()
        {
            //Arrange
            ISiteParsingStrategy siteParsingStrategy = new SiteParsingSkipWithKeyWordsStrategy(new SiteParsingStrategy(), "link1");
            string expectedUrlSecond = @"http://link2.html";
            string content = File.ReadAllText("HtmlSamples\\SinglePage\\pageWithTwoUniqueLinks.html");
            int expectedCount = 1;

            //Arrange
            var actualUrls = siteParsingStrategy.ParseContent(content, "/SinglePage/").ToList();

            //Assert
            Assert.That(actualUrls, Has.Count.EqualTo(expectedCount));
            Assert.Contains(expectedUrlSecond, actualUrls);
        }
    }
}