using System;
using System.IO;
using SiteParser.Console.Interfaces;

namespace SiteParser.Tests.Stubs
{
    public class WebClientStub : IWebClient
    {
        private const string HtmlSamplesFolder = "HtmlSamples";

        /// <summary>
        /// Returns content of sample file
        /// </summary>
        /// <param name="relatedPath">File name</param>
        /// <returns></returns>
        public string GetContent(string relatedPath)
        {
            relatedPath = relatedPath.Replace(@"http:\\", "");
            relatedPath = relatedPath.Replace(@"http://test.com/", "");
            
            string filePath = Path.Combine(HtmlSamplesFolder, relatedPath);
            if (!File.Exists(filePath))
            {
                return "";
            }

            return File.ReadAllText(filePath);
        }

        public string GetDomain(string url)
        {
            var uri = new Uri(url);
            return uri.Host;
        }

        public string GetPageAbsoluteUriWithOutName(string url)
        {
            Uri rootPageUri = new Uri(url);
            return Path.GetDirectoryName(rootPageUri.AbsoluteUri)
                .Replace("\\", "/")
                .Replace("http:/", "http://");
        }
    }
}