using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SiteParser.Console.Interfaces;

namespace SiteParser.Tests.Stubs
{
    public class WebClientEmulatorStub : IWebClient
    {
        private const string _domainPrefix = "http://test.com/";
        private const string _externalDomainPrefix = "http://test2.com/";
        private const string pagePatternPath = "HtmlSamples//pagePattern.html";
        private readonly int _emulationDepth;
        private readonly int _linksOnPage;
        private readonly int _externalLinksOnPage;
        private readonly string _aTagTemplate = "<a href='@href'>Link</a>";
        private readonly string _linkPlaceHolder = "@href";
        private readonly string _linksBlockPlaceholder = "@aBlock";
        public int ExternalGetContentCount { set; get; } = 0;

        readonly Dictionary<string,string> pages = new Dictionary<string, string>();

        public WebClientEmulatorStub(int emulationDepth, int linksOnPage, int externalLinksOnPage)
        {
            _emulationDepth = emulationDepth;
            _linksOnPage = linksOnPage;
            _externalLinksOnPage = externalLinksOnPage;

            InitPages();
        }

        private void InitPages()
        {
            string pagePatternContent = GetPattern();
            string pageLinkName = "Link";
            
            for (int pageLevel = 1; pageLevel <= _emulationDepth; pageLevel++)
            {
                StringBuilder linksBlock = new StringBuilder();
                
                for (int linkIndex = 1; linkIndex <= _linksOnPage; linkIndex++)
                {
                    linksBlock.Append(_aTagTemplate.Replace(_linkPlaceHolder, $"{_domainPrefix}{pageLinkName}{pageLevel + 1}-{linkIndex}"));
                }

                for (int linkIndex = 1; linkIndex <= _externalLinksOnPage; linkIndex++)
                {
                    linksBlock.Append(_aTagTemplate.Replace(_linkPlaceHolder, $"{_externalDomainPrefix}{pageLinkName}{pageLevel + 1}-{linkIndex}"));
                }

                for (int linkIndex = 1; linkIndex <= _linksOnPage; linkIndex++)
                {
                    pages.Add($"{pageLinkName}{pageLevel}-{linkIndex}", pagePatternContent.Replace(_linksBlockPlaceholder, linksBlock.ToString()));
                }
            }
        }

        public string GetContent(string pageName)
        {
            if (pageName.Contains(_externalDomainPrefix))
            {
                ExternalGetContentCount += 1;
            }

            if (pages.ContainsKey(pageName.Replace(_domainPrefix, "")))
            {
                return pages[pageName.Replace(_domainPrefix, "")];
            }

            return string.Empty;
        }

        public string GetDomain(string url)
        {
            var uri = new Uri(url);
            return uri.Host;
        }
        public string GetPageAbsoluteUriWithOutName(string url)
        {
            Uri rootPageUri = new Uri(url);
            return Path.GetDirectoryName(rootPageUri.AbsoluteUri)
                .Replace("\\", "/")
                .Replace("http:/", "http://");
        }

        private static string GetPattern()
        {
            if (!File.Exists(pagePatternPath))
            {
                {
                    return "";
                }
            }
            return File.ReadAllText(pagePatternPath);

        }
    }
}